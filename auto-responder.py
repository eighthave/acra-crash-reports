#!/usr/bin/env python3

import mailbox
import os
import re
import sys
import time
import urllib

from pkg_resources import parse_version
from email.header import decode_header, make_header
from email.utils import make_msgid, parseaddr, parsedate_to_datetime
from email.mime.multipart import MIMEMultipart
from email.mime.message import MIMEMessage
from email.mime.text import MIMEText


CURRENT_VERSION = '1.2.2'
MIN_OS_VERSION = '4.0'


def we_are_tracking(bug):
    return '''
Any extra information you can provide will help us track down this
bug.  We are tracking this issue here:

https://gitlab.com/fdroid/fdroidclient/issues/{bug}
'''.format(bug=bug)

fixed = '''
We have fixed this, and it is included in the {version} release.
'''.format(version='1.3')


app_version_name_pattern = re.compile(r'APP_VERSION_NAME=(\S+)')
android_version_pattern = re.compile(r'ANDROID_VERSION=(\S+)')
package_name_pattern = re.compile(r'PACKAGE_NAME=(\S+)')
brand_pattern = re.compile(r'BRAND=([\w ]+)')
phone_pattern = re.compile(r'PHONE_MODEL=([\w ]+)')
stack_trace_pattern = re.compile(r'STACK_TRACE=(.*)')
full_stack_trace_pattern = re.compile(r'STACK_TRACE=(.*\))[ \n]', re.DOTALL)
stack_trace_hash_pattern = re.compile(r'STACK_TRACE_HASH=([A-Fa-f0-9]+)')

unsent_path = '/home/hans/.thunderbird/0oxcqb7z.default/Mail/Local Folders/Unsent Messages'
outbox_path = '/home/hans/.thunderbird/0oxcqb7z.default/Mail/Local Folders/ACRA Reply Outbox'
queue_path = '/home/hans/.thunderbird/0oxcqb7z.default/Mail/Local Folders/ACRA Process Queue'

# delete the Thunderbird index files
for path in (unsent_path, outbox_path, queue_path):
    if os.path.exists(path + '.msf'):
        os.remove(path + '.msf')

outbox = mailbox.mbox(outbox_path, create=True)
queue = mailbox.mbox(queue_path, create=True)
count = 0
for key, message in queue.items():
    from_header = None
    body = ''
    try:
        if message.is_multipart():
            for part in message.walk():
                if part.get_content_type() in ('message/rfc822', 'text/plain'):
                    body += part.get_payload(decode=True).decode()
        else:
            body = message.get_payload(decode=True).decode()
        body = body.replace('\r\n', '\n').replace('\r', '\n').replace('<br>', '\n')
        #print('\n\n-=< BODY:', body, '\n\n')
    except Exception as e:
        print('fail', message['from'], str(e), file=sys.stderr)

    if not body:
        print('NO BODY FOUND', message['from'])
        continue

    stack_trace = ''
    m = full_stack_trace_pattern.search(body)
    if not m:
        m = stack_trace_pattern.search(body)
    if m:
        stack_trace = m.group(1).strip().replace('\n', ' ').replace('  ', ' ')

    m = stack_trace_hash_pattern.search(body)
    stack_trace_hash = ''
    if m:
        stack_trace_hash = m.group(1).strip()

    m = app_version_name_pattern.search(body)
    app_version_name = ''
    if m:
        app_version_name = m.group(1).strip()

    m = android_version_pattern.search(body)
    android_version = ''
    if m:
        android_version = m.group(1).strip()

    m = brand_pattern.search(body)
    brand = ''
    if m:
        brand = m.group(1).strip()

    m = package_name_pattern.search(body)
    package_name = ''
    if m:
        package_name = m.group(1).strip()

    m = phone_pattern.search(body)
    phone = ''
    if m:
        phone = m.group(1).strip()

    replybody = None
    if 'alpha' in app_version_name:
        replyheader = 'Thanks for the bug report and thank you for running alphas!\n'
    else:
        replyheader = 'Thanks for the bug report!\n'

    if app_version_name == '0.102.3':
        replyheader += '''
The old F-Droid GUI in 0.102.3 is unmaintained, and needs a maintainer!
https://gitlab.com/fdroid/fdroidclient/issues/48

'''

    if not app_version_name or not stack_trace:
        print('app_version_name and/or stack_trace missing', message['from'], message['date'])
        if not stack_trace:
            print('\n\n=========================================================')
            print('FROM', message['from'])
            print('SUBJECT', message['subject'])
            print('BODY', body.replace('\n', '<BR>'))
        replybody = None
    elif stack_trace.startswith("""java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.String org.fdroid.fdroid.data.Apk.getUrl()'"""):
        replybody = we_are_tracking(1303)
    elif stack_trace.startswith('''bindModel(AppDetailsRecyclerViewAdapter.java:9'''):
        replybody = we_are_tracking(1449)
    elif 'findApkMatchingHash(AppUpdateStatusService.java:155)' in stack_trace:
        if parse_version(app_version_name) < parse_version('1.1-alpha3'):
            replybody = '''
We have fixed this, and it is included in the v{version} release,
which is now available.  This issue is tracked here:

https://gitlab.com/fdroid/fdroidclient/issues/1305
'''.format(version=CURRENT_VERSION)
        else:
            replybody = we_are_tracking(1305)
    elif stack_trace.startswith('java.lang.IllegalStateException: Unknown loader requested: 0'):
        replybody = '''
We have fixed this, and it is included in the latest release,
which is now available.  This issue is tracked here:

https://gitlab.com/fdroid/fdroidclient/issues/1203
'''
    elif parse_version(app_version_name) < parse_version('1.0') \
         and parse_version(app_version_name) > parse_version('1.0-alpha3'):
        replybody = '''
Sorry, this was a bad one.  You can get rid of the crash by clearing
data from Settings -> Apps -> F-Droid.  That will delete your settings
and any custom repositories, but the rest of the setup will not
change.

Or you can manually download and install the {version} release as an update,
which will keep all your settings:
https://f-droid.org/packages/org.fdroid.fdroid

You can read about F-Droid 1.0 in the blog post:
https://f-droid.org/2017/10/10/fdroid-is-1.0.html
https://f-droid.org/2017/04/04/new-ux.html

For more info on the bug:
https://gitlab.com/fdroid/fdroidclient/issues/1181
'''.format(version=CURRENT_VERSION)
    elif stack_trace.startswith("""java.lang.IllegalStateException: attempt to re-open an already-closed object: SQLiteQuery: SELECT fdroid_app.name,"""):
        replybody = we_are_tracking(1230)
    elif 'Provisioner.scanAndProcess(Provisioner.java:52)' in stack_trace:
        replybody = '''
We have fixed this, and it is included in the {version} release.
This is being tracked here:
https://gitlab.com/fdroid/fdroidclient/issues/1332
'''.format(version=CURRENT_VERSION)
    elif 'App.getInstance(App.java:390)' in stack_trace:
        replybody = '''
We have fixed this, and it is included in the {version} release.
'''.format(version='1.3-alpha4')
    elif 'NotificationHelper.java:154' in stack_trace:
        replybody = we_are_tracking(1367)
    elif 'Utils.getBinaryHash(Utils.java:429)' in stack_trace \
         or 'org.fdroid.fdroid.Utils.getBinaryHash' in stack_trace \
         or '(DownloaderService.java:249)' in stack_trace:
        replybody = '''
We have fixed this, and it is included in the {version} release,
which is now available.
'''.format(version=CURRENT_VERSION)
    elif stack_trace.startswith('java.lang.IllegalArgumentException: contentIntent required: pkg=org.fdroid.fdroid'):
        replybody = '''
We have fixed this, and it is included in the {version} release.
This issue is tracked here:

https://gitlab.com/fdroid/fdroidclient/issues/1306
'''.format(version=CURRENT_VERSION)
    elif 'InstallerService.onHandleWork(InstallerService.java:66)' in stack_trace:
        replybody = '''
We have fixed this, and it is included in the {version} release,
which is now available.
'''.format(version=CURRENT_VERSION)
    elif 'KnownVulnAppListItemController.java:69' in stack_trace:
        replybody = we_are_tracking(1219)
    elif stack_trace.startswith("java.lang.IllegalStateException: Tried to upgrade or uninstall app with known vulnerability but it doesn't seem to be installed"):
        replybody = we_are_tracking(1219)
    elif stack_trace.startswith('java.lang.RuntimeException: Unable to start service org.fdroid.fdroid.net.DownloaderService@'):
        replybody = we_are_tracking(1308)
    elif 'org.apache.commons.net.util.SubnetUtils.calculate' in stack_trace:
        replybody = fixed
    elif stack_trace_hash.startswith('aa9e2cbc') \
         or stack_trace_hash.startswith('7a126a6c') \
         or stack_trace_hash.startswith('fdb4d8c2') \
         or stack_trace_hash.startswith('a36be003') \
         or stack_trace_hash.startswith('80c998cd') \
         or '''java.lang.IllegalStateException: Couldn't find installed apk''' in stack_trace:
        replybody = we_are_tracking(1435)
    elif stack_trace_hash.startswith('599662ab') \
         or stack_trace_hash.startswith('59ad6785') \
         or """uninstall(InstallerService.java:14""" in stack_trace:
        replybody = we_are_tracking(1536)
    elif stack_trace_hash.startswith('d2c8bf0f') \
         or stack_trace_hash.startswith('dadc90cf') \
         or stack_trace_hash.startswith('a9c7adc7') \
         or stack_trace_hash.startswith('1f502008') \
         or stack_trace_hash.startswith('c479e4b6') \
         or stack_trace_hash.startswith('bd5800f6') \
         or """formatDateFormat(Utils.java:""" in stack_trace:
        replybody = we_are_tracking(1541)
    elif 'org.fdroid.fdroid.installer.Installer.getUninstallScreen(' in stack_trace:
        replybody = we_are_tracking(1436)
    elif stack_trace_hash.startswith('406cdd6d') \
         or stack_trace_hash.startswith('d16ee6e1') \
         or stack_trace_hash.startswith('cdcd66b1') \
         or 'NotificationHelper.java:148' in stack_trace:
        replybody = we_are_tracking(1436)
    elif 'ExtensionInstaller.java:' in stack_trace:
        replybody = '''
The Privileged Extension root installer aka "Extension Installer" has been
removed from F-Droid.  The root install method cannot work on recent
Android versions, and is buggy.  The recommended way to install F-Droid
Privileged Extension is to flash the OTA ZIP (e.g. like flashing "gapps").
Once it is installed by flashing the OTA ZIP, F-Droid will be able to
install updates directly.  Here's the OTA ZIP package:
https://f-droid.org/packages/org.fdroid.fdroid.privileged.ota
'''
    elif stack_trace_hash.startswith('ab3563e8') \
         or stack_trace_hash.startswith('6d0c166a') \
         or stack_trace_hash.startswith('f0c5a9ba') \
         or stack_trace_hash.startswith('1a998636') \
         or 'normalizeUrl(ManageReposActivity.java' in stack_trace:
        replybody = '''
We have fixed this, and it is included in the {version} release.
'''.format(version='1.3-alpha5')
    elif parse_version(app_version_name) == parse_version('1.1-alpha2'):
        replybody = '''
Sorry, 1.1-alpha2 had a bad bug that caused it to constantly redownload
the index updates.  This is documented here:
https://gitlab.com/fdroid/fdroidclient/issues/1325

It has been fixed in v1.1, which is now available.
'''
    elif package_name in ('org.fdroid.fdroie', 'org.matkie.fdroid'):
        replybody = '''
This is a fake version of F-Droid!  Please uninstall it immediately and
install it only from the original source:  https://f-droid.org
F-Droid is free, open source software that is distributed directly from
the source without charge.

It could be malware:
* https://androidobservatory.org/app/F7C4A652EBE679C6CF8912D1B26070F23EAA450C
* https://www.virustotal.com/#/file/5962770b87a51fe9198ffdece47ca6faafad98e162275bb485833381774a29cd/detection

You should also let the website where you got this version know that they
are distributing malware.  We are also interested in hearing about where
this version of F-Droid came from so we can help figure out ways to
prevent this happening again.
'''

    msg = ''
    if app_version_name \
       and parse_version(app_version_name) < parse_version(CURRENT_VERSION):
        msg += '''

You are running an old version of F-Droid ({old}), upgrade to the
latest version ({current}).  Please report back if this is still an
issue with the upgraded version.
'''.format(old=app_version_name, current=CURRENT_VERSION)

    if 'xposed.XposedBridge' in stack_trace:
        msg += '''

It looks like you are using Xposed on this device.  Keep in mind that
this can sometimes cause strange crashes.
'''

    if parse_version(android_version) < parse_version(MIN_OS_VERSION):
        msg += '''
It looks like your devices is running Android {version}. The minimum
supported Android OS version for F-Droid is currently {min_os_version}.
We recommend that you find an update or an Android ROM project for
your device so that it can run a more recent version.

'''.format(version=android_version, min_os_version=MIN_OS_VERSION)

    if msg:
        if replybody:
            replybody += msg
        else:
            replybody = msg

    if replybody:
        mimebody = MIMEMultipart('alternative')
        mimebody.attach(MIMEText(replyheader + replybody, 'plain'))
        reply = MIMEMultipart('mixed')
        reply.attach(mimebody)
        reply.attach(MIMEMessage(message))

        reply['Message-ID'] = make_msgid()
        reply['In-Reply-To'] = message['Message-ID']
        reply['References'] = message['Message-ID']
        reply['Subject'] = 'Re: ' + (message['Subject'] or '')
        reply['To'] = message['Reply-To'] or message['From']
        reply['From'] = 'reports@f-droid.org'

        outbox.lock()
        outbox.add(reply)
        outbox.unlock()

        queue.lock()
        queue.remove(key)
        queue.unlock()

outbox.close()
queue.close()
