
import re

phone_models = (
"""    
 
PHONE_MODEL=CUBOT_NOTE_S 
APP_VERSION_NAME=1.5-alpha0 
""",

"""
PHONE_MODEL=CUBOT_POWER 
TOTAL_MEM_SIZE=118267326464 
STACK_TRACE=java.lang.RuntimeException: Unable to create service 
""",

"""
 
PHONE_MODEL=CUBOT_P20 
APP_VERSION_NAME=1.6 
""",

"""
	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:679)

PHONE_MODEL=CUBOT_MANITO
APP_VERSION_NAME=1.7.1
""",

"""
ANDROID_VERSION=8.1.0 
PHONE_MODEL=CUBOT_J3 
AVAILABLE_MEM_SIZE=9906003968 
""",

"""
ANDROID_VERSION=8.1.0 
PHONE_MODEL=CUBOT_J3 
AVAILABLE_MEM_SIZE=9905938432 
""",
)

both = (

"""
BRAND=CUBOT 
AVAILABLE_MEM_SIZE=9930539008 
STACK_TRACE_HASH=d12da6f1 
PHONE_MODEL=CUBOT MAGIC 
PACKAGE_NAME=org.fdroid.fdroid 
""",

"""
 
PHONE_MODEL=CUBOT_MANITO 
APP_VERSION_NAME=1.5.1 
STACK_TRACE_HASH=4282a856 
USER_COMMENT= 
AVAILABLE_MEM_SIZE=5313519616 
CUSTOM_DATA= 
BRAND=CUBOT 
ANDROID_VERSION=6.0 
""",

"""
 
TOTAL_MEM_SIZE=13458997248 
BRAND=CUBOT 
DISPLAY=0.currentSizeRange.smallest=[480,444] 
APP_VERSION_NAME=1.6 
PHONE_MODEL=CUBOT_J3 
STACK_TRACE_HASH=db9c0341 
""",

"""
BRAND=CUBOTTOTAL_MEM_SIZE=27555057664PHONE_MODEL=CUBOT H3STACK_TRACE_HASH=1954132b
""",

"""
PACKAGE_NAME=org.fdroid.fdroid PHONE_MODEL=CUBOT H3 BRAND=CUBOT STACK_TRACE=java.lang.RuntimeException: Unable to start activity
""",

"""
BRAND=CUBOT 
DISPLAY=0.currentSizeRange.smallest=[480,444] 
PHONE_MODEL=CUBOT_J3 
AVAILABLE_MEM_SIZE=9905938432 
""",
)    

products = (
"""
PRODUCT=CUBOT_MAGIC 
APP_VERSION_NAME=1.5.1 
ANDROID_VERSION=7.0 
BRAND=CUBOT 
""",

"""
STACK_TRACE_HASH=1954132b
BRAND=CUBOT
PRODUCT=CUBOT_H3
DISPLAY=0.currentSizeRange.smallest=[720,666]
""",

"""
TOTAL_MEM_SIZE=12000641024
PRODUCT=CUBOT_MANITO
DISPLAY=0.currentSizeRange.smallest=[720,672]
""",
    
"""
PRODUCT=CUBOT_J3 
 
--  
Diese Nachricht wurde von meinem Android-Gerät mit K-9 Mail gesendet.
""",

"""
at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:857) 
 
PRODUCT=CUBOT_POWER 
ANDROID_VERSION=8.1.0 
APP_VERSION_NAME=1.6 
STACK_TRACE_HASH=9daf307e 
BRAND=CUBOT 
CUSTOM_DATA= 
AVAILABLE_MEM_SIZE=92473188352 
""",

"""TOTAL_MEM_SIZE=12264865792 
PRODUCT=CUBOT_NOTE_S 
DISPLAY=0.currentSizeRange.smallest=[720,672] 
""",

"""
PRODUCT=CUBOT_J3 
USER_COMMENT= 
""",

"""
TOTAL_MEM_SIZE=12000641024 
PRODUCT=CUBOT_MANITO 
DISPLAY=0.currentSizeRange.smallest=[720,672] 
STACK_TRACE=java.util.ConcurrentModificationException 
	at java.util.HashMap$HashIterator.nextEntry(HashMap.java:787) 
	at java.util.HashMap$ValueIterator.next(HashMap.java:819) 
	at org.fdroid.fdroid.NotificationHelper.updateStatusLists(NotificationHelper.java:142) 
	at org.fdroid.fdroid.NotificationHelper.access$100(NotificationHelper.java:36) 
	at org.fdroid.fdroid.NotificationHelper$1.onReceive(NotificationHelper.java:94) 
	at android.support.v4.content.LocalBroadcastManager.executePendingBroadcasts(LocalBroadcastManager.java:311) 
	at android.support.v4.content.LocalBroadcastManager.access$000(LocalBroadcastManager.java:47) 
	at android.support.v4.content.LocalBroadcastManager$1.handleMessage(LocalBroadcastManager.java:120) 
	at android.os.Handler.dispatchMessage(Handler.java:111) 
	at android.os.Looper.loop(Looper.java:207) 
	at android.app.ActivityThread.main(ActivityThread.java:5765) 
	at java.lang.reflect.Method.invoke(Native Method) 
	at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:789) 
	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:679) 
BRAND=CUBOT 
""",
)

brand_pattern = re.compile(r'BRAND=([\w -]{3,}?) *(?:[A-Z]{3,}[A-Z_]+[A-Z]=|\n)')
phone_model_pattern = re.compile(r'PHONE_MODEL=([\w -]{3,}?) *(?:[A-Z]{3,}[A-Z_]+[A-Z]=|\n)')

for i in both:
    m = brand_pattern.search(i.replace('\r', ''))
    if m:
        print('brand', m.group(1), m.group())
    else:
        print('ERROR:', i)

for i in both + phone_models:
    m = phone_model_pattern.search(i.replace('\r', ''))
    if m:
        print('phone_model', m.group(1))
    else:
        print('ERROR:', i)